#! /usr/bin/env python
import subprocess
import struct
import array
class TriMultPoly:
     def __init__(self, executable):
         self.process = subprocess.Popen([executable, '-d'], stdin = subprocess.PIPE, stderr = subprocess.PIPE)
     def __write(self, fmt, data):
         self.process.stdin.write(struct.pack(fmt, data))
     def triangulate(self, data, use_dt = True, use_min_set = True, use_normal = False, area_weight = 0, edge_weight = 0, dihedral_weight = 0, min_max_dihedral = 1):
         self.__write('b', use_dt)
         self.__write('b', use_min_set)
         self.__write('b', use_normal)
         self.__write('d', area_weight)
         self.__write('d', edge_weight)
         self.__write('d', dihedral_weight)
         self.__write('d', min_max_dihedral)
         nb_curve = len(data)
         self.__write('i', nb_curve)
         nb_vertices = 0
         nbp = 6 if use_normal else 3
         for i in xrange(nb_curve):
             nb_vertices += len(data[i])
         nb_vertices /= nbp
         self.__write('i', nb_vertices)
         for i in xrange(nb_curve):
             self.__write('i', len(data[i]) / nbp)
             a = data[i]
             try:
                 f = a.tofile
             except AttributeError:
                 f = array.array('d', a).tofile
             f(self.process.stdin)
         self.process.stdin.flush()
         buf = self.process.stderr.read(4)
         nb_triangles = struct.unpack('i', buf)[0]
         triangles = array.array('i')
         triangles.fromfile(self.process.stderr, nb_triangles * 3)
         return triangles
     def release(self):
         self.__write('i', -1)

if __name__ == "__main__":
     t = TriMultPoly('./TriMultPoly')
     nodes = [
4.502539,-1.103106,0.548982,
2.502539,-1.103106,0.548982,
2.362264,-1.014786,0.548982,
2.258357,-0.853729,0.548982,
2.242771,-0.604352,0.548982,
2.398632,-0.308217,0.548982,
2.845433,-0.053644,0.548982,
3.396141,-0.017276,0.548982,
3.941654,-0.038058,0.548982,
4.372869,0.097022,0.548982,
4.585879,0.455501,0.548982,
4.497558,0.658120,0.548982,
4.081929,0.855544,0.548982,
3.650714,0.954256,0.548982,
3.048052,0.969842,0.548982,
2.299920,0.881521,0.548982,
2.299920,0.881521,-1.451018,
3.048052,0.969842,-1.451018,
3.650714,0.954256,-1.451018,
4.081929,0.855544,-1.451018,
4.497558,0.658120,-1.451018,
4.585879,0.455501,-1.451018,
4.372869,0.097022,-1.451018,
3.941654,-0.038058,-1.451018,
3.396141,-0.017276,-1.451018,
2.845433,-0.053644,-1.451018,
2.398632,-0.308217,-1.451018,
2.242771,-0.604352,-1.451018,
2.258357,-0.853729,-1.451018,
2.362264,-1.014786,-1.451018,
2.502539,-1.103106,-1.451018,
4.502539,-1.103106,-1.4510180
     ]
     import timeit
     t1 = timeit.default_timer()
     n = 30
     for i in xrange(n):
         t.triangulate([nodes])
     t2 = timeit.default_timer()
     print (t2-t1) / n
     t.release()
